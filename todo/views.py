# -*- coding: utf-8 -*-
"""
    todo.views
    ~~~~~~~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: views
"""

from flask import render_template, request, session, abort, redirect, url_for, flash

from todo import app
from todo.auth import requires_auth, login_user, logout_user, check_authentication


@app.route('/', methods=['GET'])
@app.route('/index.html', methods=['GET'])
def index():
    return render_template('index.html')


@app.route('/login', methods=['POST','GET'])
def login():
    error = None
    if request.method == 'GET':
        return render_template('login.html')

    elif request.method == 'POST':
        email    = request.form['email']
        password = request.form['password']

        if check_authentication(email, password):
            login_user(email)
            return redirect(request.args.get("next") or url_for('index'))

    return render_template('index.html'), 401


@app.route('/logout', methods=['POST'])
@requires_auth
def logout():
    # remove the username from the session if it's there
    logout_user()
    return redirect(request.args.get("next") or url_for('index'))



