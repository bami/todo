# -*- coding: utf-8 -*-
"""
    todo.logging
    ~~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: logging
"""
import logging

# logger
logger = logging.getLogger()
fh = logging.FileHandler('debug.log')
logger.addHandler(fh)
logger.setLevel(logging.DEBUG)

