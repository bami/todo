# -*- coding: utf-8 -*-
"""
    todo.database
    ~~~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: database handler
"""

from sqlalchemy import create_engine
from sqlalchemy.engine import reflection
from sqlalchemy.orm import scoped_session, sessionmaker
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.schema import MetaData, Table, DropTable, \
        ForeignKeyConstraint, DropConstraint
import todo.config

DATABASE_URI = todo.config.SQLALCHEMY_DATABASE_URI

engine = create_engine(DATABASE_URI, convert_unicode=True)
db_session = scoped_session(sessionmaker(autocommit=False, 
    autoflush=False, bind=engine))

Base = declarative_base()
Base.query = db_session.query_property()
Base.metadata.bind = engine

def insert_init_data():

    import todo.models
    from todo.logger import logger
    # create admin user
    db_session = todo.database.db_session
    admin = todo.models.User(email=u'admin', password=u'admin', admin=True)
    db_session.add(admin)
    db_session.commit()

    logger.debug("user %s created" % admin)
    # create dummy user
    test = todo.models.User(email=u'user', password=u'user', admin=False)
    db_session.add(test)
    db_session.commit()
    logger.debug("user %s created" % test)

def init():
    # import all modules here that might define models so that
    # they will be registered properly on the metadata.  Otherwise
    # you will have to import them first before calling init()
    import todo.models
    Base.metadata.create_all(bind=engine)


def list_tables():
    """
    return database table names
    """
    metadata = MetaData()
    return metadata.tables.keys()


def drop():
    """
    drop database
    """

    Base.metadata.drop_all(bind=engine)


def wipe():
    """
    clear all data in database
    """

    conn = engine.connect()

    # the transaction only applies if the DB supports
    # transactional DDL, i.e. Postgresql, MS SQL Server
    trans = conn.begin()

    inspector = reflection.Inspector.from_engine(engine)

    # gather all data first before dropping anything.
    # some DBs lock after things have been dropped in
    # a transaction.
    metadata = MetaData()

    tbs = []
    all_fks = []

    for table_name in inspector.get_table_names():
        fks = []
        for fk in inspector.get_foreign_keys(table_name):
            if not fk['name']:
                continue
            fks.append(
                ForeignKeyConstraint((),(),name=fk['name'])
            )
        t = Table(table_name,metadata,*fks)
        tbs.append(t)
        all_fks.extend(fks)

    for fkc in all_fks:
        conn.execute(DropConstraint(fkc))

    for table in tbs:
        conn.execute(DropTable(table))

    trans.commit()

