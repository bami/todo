TodoApp.factory('Todo', function($resource) {
    return $resource('/api/todoitem/:id', { id: '@id' }, { update: { method: 'PUT' } });
});


var TodoListCtrl = function ($scope, $location, Todo) {

    $scope.sort_order = 'id';
    $scope.desc = false;

    var make_query = function() {
        var q = {order_by: [{field: $scope.sort_order, direction: $scope.sort_desc ? "desc": "asc"}]};
        if ($scope.query) {
            q.filters = [{name: "todo", op: "like", val: "%" + $scope.query + "%"} ];
        }
        return angular.toJson(q);
    }

    $scope.reset = function() {
        $scope.page = 1;
        $scope.search();
    };

    $scope.remove = function () {
        var itemId = this.item.id;
        Todo.remove({ id: itemId }, function () {
            $("#item_" + itemId).fadeOut();
        });
    };

    $scope.search = function () {
        var res = Todo.get(
            { page: $scope.page, q: make_query() },
            function () {
                $scope.no_more = res.page == res.total_pages;
                if (res.page==1) { $scope.todos=[]; }
                $scope.todos = $scope.todos.concat(res.objects);
            }
        );
    };
    $scope.sort_by = function (ord) {
        if ($scope.sort_order == ord) { $scope.sort_desc = !$scope.sort_desc; }
        else { $scope.sort_desc = false; }
        $scope.sort_order = ord;
        $scope.reset();
    };

    $scope.do_show = function (asc, col) {
        return (asc != $scope.sort_desc) && ($scope.sort_order == col);
    };

    $scope.show_more = function () { return !$scope.no_more; };


    $scope.limit = 20;
    $scope.reset();
};


var TodoCreateCtrl = function ($scope, $location, Todo) {
    $scope.save = function () {
        Todo.save($scope.item, function() {
            $location.path('/');
        });
    };
};

var TodoEditCtrl = function ($scope, $routeParams, $location, Todo) {
    $scope.item = Todo.get({ id: $routeParams.itemId });

    $scope.save = function () {
        Todo.update({id: $scope.item.id}, $scope.item, function () {
            $location.path('/');
        });
    };
};

