var TodoApp = angular.module("TodoApp", ["ngResource"]).
    config(function($routeProvider) {
        $routeProvider.
            when('/', { controller: TodoListCtrl, templateUrl: '/static/todo_list.html' }).
            when('/todo/new', { controller: TodoCreateCtrl, templateUrl: '/static/todo_detail.html' }).
            when('/todo/edit/:itemId', { controller: TodoEditCtrl, templateUrl: '/static/todo_detail.html' }).
            when('/user', { controller: UserListCtrl, templateUrl: '/static/user_list.html' }).
            when('/user/new', { controller: UserCreateCtrl, templateUrl: '/static/user_detail.html' }).
            when('/user/edit/:itemId', { controller: UserEditCtrl, templateUrl: '/static/user_detail.html' }).

            otherwise({ redirectTo: '/' });
    });


TodoApp.directive('sorted', function() {
    return {
        scope: true,
        transclude: true,
        template: '<a ng-click="do_sort()" ng-transclude></a>' +
    '<span ng-show="do_show(true)"><i class="icon-circle-arrow-down"></i></span>' +
    '<span ng-show="do_show(false)"><i class="icon-circle-arrow-up"></i></span>',
        controller: function($scope, $element, $attrs) {
            $scope.sort = $attrs.sorted;

            $scope.do_sort = function() { $scope.sort_by($scope.sort); };

            $scope.do_show = function(asc) {
                return (asc != $scope.sort_desc) && ($scope.sort_order == $scope.sort);
            }
         }
    }
});
