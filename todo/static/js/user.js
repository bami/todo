TodoApp.factory('User', function($resource) {
    return $resource('/api/user/:id', { id: '@id' }, { update: { method: 'PUT' } });
});

var UserListCtrl = function ($scope, $location, User) {

    $scope.sort_order = 'id';
    $scope.desc = false;

    var make_query = function() {
        var q = {order_by: [{field: $scope.sort_order, direction: $scope.sort_desc ? "desc": "asc"}]};
        if ($scope.query) {
            q.filters = [{name: "email", op: "like", val: "%" + $scope.query + "%"} ];
        }
        return angular.toJson(q);
    }

    $scope.reset = function() {
        $scope.page = 1;
        $scope.search();
    };

    $scope.remove = function () {
        var itemId = this.item.id;
        User.remove({ id: itemId }, function () {
            $("#item_" + itemId).fadeOut();
        });
    };

    $scope.search = function () {
        var res = User.get(
            { page: $scope.page, q: make_query() },
            function () {
                $scope.no_more = res.page == res.total_pages;
                if (res.page==1) { $scope.users=[]; }
                $scope.users = $scope.users.concat(res.objects);
            }
        );
    };
    $scope.sort_by = function (ord) {
        if ($scope.sort_order == ord) { $scope.sort_desc = !$scope.sort_desc; }
        else { $scope.sort_desc = false; }
        $scope.sort_order = ord;
        $scope.reset();
    };

    $scope.do_show = function (asc, col) {
        return (asc != $scope.sort_desc) && ($scope.sort_order == col);
    };

    $scope.show_more = function () { return !$scope.no_more; };


    $scope.limit = 20;
    $scope.reset();
};


var UserCreateCtrl = function ($scope, $location, User) {
    $scope.save = function () {
        User.save($scope.item, function() {
            $location.path('/index.html#/user');
        });
    };
};

var UserEditCtrl = function ($scope, $routeParams, $location, User) {
    $scope.item = User.get({ id: $routeParams.itemId });

    $scope.save = function () {
        User.update({id: $scope.item.id}, $scope.item, function () {
            $location.path('/index.html#/user');
        });
    };
};

