# -*- coding: utf-8 -*-
"""
    todo.config
    ~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: config file
"""
import os

# Flask 
DEBUG = True
SECRET_KEY = os.urandom(24)

# database
SQLALCHEMY_DATABASE_URI = 'sqlite:////tmp/todo.db'
