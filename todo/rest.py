# -*- coding: utf-8 -*-
"""
    todo.rest
    ~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: rest api processors
"""

from flask import session
from flask.ext.restless import ProcessingException
from todo.models import TodoItem
from todo.logger import logger

def check_login(instance_id=None, **kw):
    if not session.get('uid'):
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


# user processors
def get_single_user(instance_id=None, **kw):
    """Accepts a single argument, `instance_id`, the primary key of the
    instance of the model to get.

    """
    uid = session.get('uid')
    admin = session.get('admin')

    if not ((instance_id == uid) or admin):
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def get_many_user(search_params=None, **kw):
    """Accepts a single argument, `search_params`, which is a dictionary
    containing the search parameters for the request.

    """

    admin = session.get('admin')

    if not admin:
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def patch_single_user(instance_id=None, data=None, **kw):
    """Accepts two arguments, `instance_id`, the primary key of the
    instance of the model to patch, and `data`, the dictionary of fields
    to change on the instance.

    """
    uid = session.get('uid')
    admin = session.get('admin')

    if not ((instance_id == uid) or admin):
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def post_user(data=None, **kw):
    """Accepts a single argument, `data`, which is the dictionary of
    fields to set on the new instance of the model.

    """
    admin = session.get('admin')

    if not admin:
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def delete_user(instance_id=None, **kw):
    """Accepts a single argument, `instance_id`, which is the primary key
    of the instance which will be deleted.

    """

    uid = session.get('uid')
    admin = session.get('admin')

    if not ((instance_id == uid) or admin):
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


# todo processors

def get_single_todo(instance_id=None, **kw):
    """Accepts a single argument, `instance_id`, the primary key of the
    instance of the model to get.

    """
    uid = session.get('uid')

    todo = TodoItem.query.filter(TodoItem.id == instance_id).first()
    owner_id = u'%i' % todo.owner_id
    if not owner_id == uid:
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def get_many_todo(search_params=None, **kw):
    """Accepts a single argument, `search_params`, which is a dictionary
    containing the search parameters for the request.

    """
    uid = session.get('uid')
    admin = session.get('admin')

    if admin:
        return

    # create uid filter
    filt = {'name': 'owner_id',
            'op'  : 'eq',
            'val' : uid}
    # Check if there are any filters there already.
    if 'filters' not in search_params:
        search_params['filters'] = []
    # *Append* your filter to the list of filters.
    search_params['filters'].append(filt)



def patch_single_todo(instance_id=None, data=None, **kw):
    """Accepts two arguments, `instance_id`, the primary key of the
    instance of the model to patch, and `data`, the dictionary of fields
    to change on the instance.

    """
    uid = session.get('uid')

    todo = TodoItem.query.filter(TodoItem.id == instance_id).first()
    todo_owner_id = u'%i' % todo.owner_id
    if not todo_owner_id == uid:
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


def post_todo(data=None, **kw):
    """
    force owner
    :param data: the dictionary of fields to set on the new instance of the model
    :param kw:
    :return:
    """

    uid = session.get('uid')
    data[u'owner_id'] = uid


def delete_todo(instance_id=None, **kw):
    """
    check owner

    :param instance_id: the primary key of the instance which will be deleted.
    :param kw:
    """

    uid = session.get('uid')

    todo = TodoItem.query.filter(TodoItem.id == instance_id).first()
    todo_owner_id = u'%i' % todo.owner_id
    if not todo_owner_id == uid:
        raise ProcessingException(message='Not Authorized',
                                  status_code=401)


