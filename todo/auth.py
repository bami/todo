# -*- coding: utf-8 -*-
"""
    todo.auth
    ~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: authentication module
"""

from functools import wraps
from flask import request, Response, session, abort
from todo.models import User
from todo.logger import logger

def login_user(email):
    user = User.query.filter(User.email == email).first()
    logger.debug("user %s logged in" % user)
    session['uid']   = u'%i' % user.id
    session['email'] = user.email
    session['admin'] = user.admin

def logout_user():
    session['uid']   = None
    session['email'] = None
    session['admin'] = None



def check_authentication(email, password):
    '''
    :param email:
    :param password:
    :return: username/password combination valid
    '''
    user = User.query.filter(User.email == email).first()
    logger.debug("user %s check" % user)
    return user and user.password == password


def authenticate():
    """Sends a 401 response that enables basic auth"""
    abort(401)


def requires_auth(f):
    '''
    Decorator function to assure authentication
    :param f: decorated function
    :return: return value of f if authorized
    '''
    @wraps(f)
    def decorated(*args, **kwargs):
        #auth = request.authorization
        #if not auth or not check_auth(auth.username, auth.password):
        if not session.get('uid'):
            return authenticate()
        return f(*args, **kwargs)
    return decorated

