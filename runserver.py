# -*- coding: utf-8 -*-
"""
    todo
    ~~~~~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: project
"""

import sys
import os

from todo import *
import  todo.database 

def show_help():
    print """usage %s 
    --help      this help message
    --run       run standalone server
    --create    initialize database
    --wipe      wipe database
""" % sys.argv[0]
 
if __name__ == '__main__':
    try:
        argv = sys.argv[1]
        if argv == '--help':
            show_help()
        elif argv == '--run':
            print "running server."
            # Bind to PORT if defined, otherwise default to 5000.
            port = int(os.environ.get('PORT', 5000))
            # start the flask loop
            app.run(host='0.0.0.0', port=port)
        elif argv == '--create':
            print "creating database."
            todo.database.init()
            todo.database.insert_init_data()
        elif argv == '--wipe':
            print "wiping database."
            todo.database.wipe()
        else:
            print("invalid paramter: run %s --help" % sys.args[0])
    except Exception, e:
        print e
        show_help()
        exit()

