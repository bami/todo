# -*- coding: utf-8 -*-
"""
    init_db
    ~~~~~~~

    :copyright: (c) 2013 by Bastian Migge
    :license: BSD3, see LICENSE for more details.
    :description: initialize database
"""
import json
import requests

headers = {'Content-Type': 'application/json'}
base_url = 'http://127.0.0.1:5000/api'


# user
user_url = base_url + '/user'

data = dict(email='user1', 
            password='password',
            admin=True)
response = requests.post(user_url, data=json.dumps(data), headers=headers)
assert response.status_code == 201

# Make a GET request for the entire collection.
response = requests.get(user_url, headers=headers)
assert response.status_code == 200
print(response.content)


### TODOs ###
todo_url = base_url + '/todoitem'

# Make a POST request to create an object in the database.
data = dict(todo='task1', priority=1, due_date='1-2-1923')
response = requests.post(todo_url, data=json.dumps(data), headers=headers)
assert response.status_code == 201

# Make a GET request for the entire collection.
response = requests.get(todo_url, headers=headers)
assert response.status_code == 200
print(response.content)

# Use query parameters to make a search. Make sure to convert the value of `q`
# to a string.
filters = [dict(todo='ta', op='like', val='%y%')]
params = dict(q=json.dumps(dict(filters=filters)))
response = requests.get(todo_url, params=params, headers=headers)
assert response.status_code == 200
print(response.content)

